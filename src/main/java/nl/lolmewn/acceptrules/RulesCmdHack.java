package nl.lolmewn.acceptrules;

import nl.lolmewn.acceptrules.commands.RulesCommand;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 * @author Lolmewn
 */
public class RulesCmdHack implements Listener {

    private final Main main;

    RulesCmdHack(Main main) {
        this.main = main;
        main.getServer().getPluginManager().registerEvents(this, main);
    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
        if (event.getMessage().startsWith("/rules")) {
            event.setCancelled(true);
            RulesCommand cmd = new RulesCommand(main);
            String[] args = event.getMessage().split(" ");
            if (args.length == 0 || args.length == 1) {
                cmd.onCommand(event.getPlayer(), null, "rules", new String[0]);
                return;
            }
            String[] newArgs = new String[args.length - 1];
            System.arraycopy(args, 1, newArgs, 0, args.length - 1);
            cmd.onCommand(event.getPlayer(), null, "rules", newArgs);
        }
    }

}
