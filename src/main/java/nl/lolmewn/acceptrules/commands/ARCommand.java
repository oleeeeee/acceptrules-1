package nl.lolmewn.acceptrules.commands;

import nl.lolmewn.acceptrules.Main;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

import java.util.List;

/**
 * @author Lolmewn
 */
public class ARCommand implements CommandExecutor {

    private final Main plugin;

    public ARCommand(Main plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 0) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("You can only use this command as a player!");
                return true;
            }
            Player player = (Player) sender;
            if (plugin.getAcceptedPlayers().contains(player.getUniqueId().toString())) {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&',
                        plugin.getConfig().getString("AcceptedAllreadyMsg", "You have already accepted the rules!")));
                return true;
            }
            if (plugin.getUsersWhoReadRules().containsKey(player.getName())) {
                if (plugin.getConfig().getBoolean("usePagination", true)) {
                    List<String> acceptedPages = plugin.getUsersWhoReadRules().get(player.getName());
                    for (String page : plugin.getRulesManager().keySet()) {
                        if (!acceptedPages.contains(page)) {
                            sender.sendMessage(ChatColor.RED + "You haven't read some rules yet! Page " + page + ", for example.");
                            return true;
                        }
                    }
                }
                plugin.saveUser(player.getUniqueId().toString());
                plugin.getAcceptedPlayers().add(player.getUniqueId().toString());
                player.sendMessage(ChatColor.translateAlternateColorCodes('&',
                        plugin.getConfig().getString("AcceptedMsg", "You have succesfully accepted the rules! Have fun!")));
                if (plugin.getConfig().getBoolean("TpAfterAccept", true)) {
                    World w = plugin.getServer().getWorld(plugin.getConfig().getString("TpWorld"));
                    if (w == null) {
                        w = plugin.getServer().getWorlds().get(0);
                    }
                    Location loc = new Location(w,
                            plugin.getConfig().getDouble("TpPositionX", 0),
                            plugin.getConfig().getDouble("TpPositionY", 0),
                            plugin.getConfig().getDouble("TpPositionZ", 0),
                            plugin.getConfig().getLong("TpPositionYaw", 0),
                            plugin.getConfig().getLong("TpPositionPitch", 0));
                    if (loc.getBlockX() == 0 && loc.getBlockY() == 0 && loc.getBlockZ() == 0) {
                        plugin.getLogger().warning("No teleport location has been set yet. Please do so using /acceptrules settp");
                    } else {
                        player.teleport(loc, TeleportCause.COMMAND);
                    }
                }
                List<String> pCommands = plugin.getConfig().getStringList("onAccept.commands");
                if (!pCommands.isEmpty()) {
                    for (String command : pCommands) {
                        player.performCommand(command.replace("%PLAYER%", player.getName()).replace("%player%", player.getName()));
                    }
                }
                List<String> cCommands = plugin.getConfig().getStringList("onAccept.consoleCommands");
                if (!cCommands.isEmpty()) {
                    for (String command : cCommands) {
                        if (command.equals("command1")) {
                            continue;
                        }
                        String comm = command.replace("%PLAYER%", player.getName()).replace("%player%", player.getName());
                        plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), comm);
                    }
                }
                plugin.getServer().getConsoleSender().sendMessage(ChatColor.GREEN + player.getName() + " has accepted the rules!");
                if (plugin.getConfig().getBoolean("NotifyOPs", true)) {
                    plugin.getServer().broadcast(ChatColor.GREEN + player.getName() + " has accepted the rules!", "acceptrules.notifyonaccept");
                }
                return true;
            }
            player.sendMessage(ChatColor.translateAlternateColorCodes('&',
                    plugin.getConfig().getString("MustReadRules", "&4You must read the &6rules &4in order to accept them!")));
            return true;
        }
        if (args[0].equals("settp")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("You can only use this command as a player!");
                return true;
            }
            if (!sender.hasPermission("acceptrules.settp")) {
                sender.sendMessage("You do not have permissions to do this!");
                return true;
            }
            Player p = (Player) sender;
            plugin.getConfig().set("TpWorld", p.getWorld().getName());
            plugin.getConfig().set("TpPositionX", p.getLocation().getX());
            plugin.getConfig().set("TpPositionY", p.getLocation().getY());
            plugin.getConfig().set("TpPositionZ", p.getLocation().getZ());
            plugin.getConfig().set("TpPositionYaw", p.getLocation().getYaw());
            plugin.getConfig().set("TpPositionPitch", p.getLocation().getPitch());
            plugin.saveConfig();
            p.sendMessage(ChatColor.GREEN + "Don't worry, I got this!");
            p.sendMessage(ChatColor.YELLOW + "TP position changed and saved.");
            return true;
        }
        if (args[0].equalsIgnoreCase("setspawn")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("You can only use this command as a player!");
                return true;
            }
            if (!sender.hasPermission("acceptrules.setspawn")) {
                sender.sendMessage("You do not have permissions to do this!");
                return true;
            }
            Player p = (Player) sender;
            plugin.getConfig().set("SpawnWorld", p.getWorld().getName());
            plugin.getConfig().set("SpawnPositionX", p.getLocation().getX());
            plugin.getConfig().set("SpawnPositionY", p.getLocation().getY());
            plugin.getConfig().set("SpawnPositionZ", p.getLocation().getZ());
            plugin.getConfig().set("SpawnPositionYaw", p.getLocation().getYaw());
            plugin.getConfig().set("SpawnPositionPitch", p.getLocation().getPitch());
            plugin.saveConfig();
            p.sendMessage(ChatColor.GREEN + "Don't worry, I got this!");
            p.sendMessage(ChatColor.YELLOW + "Spawn position changed and saved.");
            return true;
        }
        if (args[0].equalsIgnoreCase("reload")) {
            if (!sender.hasPermission("acceptrules.reload")) {
                sender.sendMessage("You do not have permissions to do this!");
                return true;
            }
            sender.sendMessage(ChatColor.GREEN + "Reloading plugin...");
            plugin.getRulesManager().clear();
            plugin.getAcceptedPlayers().clear();
            plugin.loadRules();
            plugin.loadUsers();
            plugin.reloadConfig();
            sender.sendMessage(ChatColor.GREEN + "All systems up and running, we're ready to go!");
            return true;
        }
        if (args[0].equalsIgnoreCase("player")) {
            if (!sender.hasPermission("acceptrules.acceptForOthers")) {
                sender.sendMessage("You do not have permissions to do this!");
                return true;
            }
            if (args.length == 1) {
                sender.sendMessage("Correct usage: /acceptrules player <player>");
                sender.sendMessage("Accepts the rules for player <player>");
                return true;
            }
            Player find = plugin.getServer().getPlayer(args[1]);
            if (find == null) {
                sender.sendMessage("Player by that name not found. (is he online?)");
                return true;
            }
            if (plugin.getAcceptedPlayers().contains(find.getUniqueId().toString())) {
                sender.sendMessage(find.getName() + " has already accepted the rules!");
                return true;
            }
            find.performCommand("acceptrules");
            sender.sendMessage("Performed /acceptulres on " + find.getName());
            return true;
        }
        if (args[0].equalsIgnoreCase("reset")) {
            if (!sender.hasPermission("acceptrules.reset")) {
                sender.sendMessage("You do not have permissions to do this!");
                return true;
            }
            if (args.length == 1) {
                sender.sendMessage("Correct usage: /acceptrules reset <playername|all>");
                return true;
            }
            String who = args[1];
            if (who.equalsIgnoreCase("all")) {
                this.plugin.getUsersWhoReadRules().clear();
                this.plugin.getAcceptedPlayers().clear();
                this.plugin.saveUsers();
                sender.sendMessage("All users have been cleared. Everyone has to re-accept the rules.");
                return true;
            } else {
                OfflinePlayer op = this.plugin.getServer().getOfflinePlayer(who);
                if (!this.plugin.getAcceptedPlayers().contains(op.getUniqueId().toString())) {
                    sender.sendMessage("User with the name " + op.getName() + " either not found or hasn't accepted the rules yet");
                    return true;
                }
                this.plugin.getAcceptedPlayers().remove(op.getUniqueId().toString());
                this.plugin.getUsersWhoReadRules().remove(op.getName());
                sender.sendMessage("Player " + op.getName() + " has to re-accept the rules.");
            }
            return true;
        }
        sender.sendMessage("I'm not sure what you're trying to achieve here...");
        sender.sendMessage("I mean, there's really no such command!");
        return true;
    }
}
